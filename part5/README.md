# Part 5 - cleanup your goroutines

In the approach provided in part 4 (and maybe yours) we spin up
goroutines and read from a given channel. However, when the work has
finished we have a goroutine leak because the `select` statements have
suspended the goroutine indefinitely.

## Goal

* Write a test to prove that we have a goroutine leak
* Provide a mechanism to exit our suspended `select` statements when
all the work has been finished

## Acceptance criteria

* We should have the same number of goroutines prior to running our work
as after

## Learning outcomes

* for-select
* Cancellation
* Closing a channel as a sentinel value
