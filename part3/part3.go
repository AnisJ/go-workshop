package part3

import "sync"

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	jobs []job
}

func NewScheduler() *Scheduler {
	return &Scheduler{}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

func (s *Scheduler) Run() []int {
	totalWork := len(s.jobs)
	results := make([]int, totalWork)

	wg := sync.WaitGroup{}
	wg.Add(totalWork)

	for i, j := range s.jobs {
		// We use the `go` keyword to run our anonymous function in a goroutine
		// Goroutines are a huge topic, see https://golang.org/doc/effective_go.html#goroutines for a starter
		go func(i int, j job) {
			// Typically you defer work within a goroutine that you want to make sure runs regardless of what happens
			// such as a panic. See https://golang.org/doc/effective_go.html#defer
			defer wg.Done()

			results[i] = j.w(j.args...)
		}(i, j) // when launching goroutines in a range loop we need to copy the values otherwise the closure will simply
		// use the last i, j pair from the range loop
	}

	wg.Wait()

	return results
}
