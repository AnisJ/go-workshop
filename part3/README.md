# Part 3 - do work concurrently

While you may have already done this in part 2, we'll assume that you
simply ranged over the scheduled work and synchronously called the
functions in order to get their results

## Goal

* Without changing the tests, run the scheduled work concurrently

## Acceptance criteria

* If you didn't manage to benchmark your implementation in part 2, copy
the given one from this repo and confirm that part 3 is approximately
100 times as fast

## Learning outcomes

* Goroutines
* defer
* Closures
