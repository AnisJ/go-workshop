# Part 1 - lazily schedule 

## Goal

* Create a package that we can lazily add functions to with the
signature `func(...int) int` and their arguments. We may do this `n`
times.

## Acceptance criteria

* Proof that we can schedule multiple functions and their arguments and
that they're not called when scheduled

## Learning outcomes

* Go modules
* Declaring a package
* Types
* Functions
* Structs
* The "New pattern"
* Receivers