package part4b

import "runtime"

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	maxgr int
	jobs  []job
}

func NewScheduler(maxgr int) *Scheduler {
	if maxgr == 0 {
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr: maxgr,
	}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

type result struct {
	value,
	position int
}

type resultStream chan result

func (s *Scheduler) Run() []int {
	totalWork := len(s.jobs)
	results := make([]int, totalWork)
	// Create a buffered channel with a capacity of the number of goroutines we would like to restrict to
	ws := make(chan struct{}, s.maxgr)
	rs := make(resultStream, totalWork)

	defer close(rs)
	// Make sure we clean up at the end of our run
	defer close(ws)

	for i, j := range s.jobs {
		// We'll be able to keep writing until we hit the max number we're allowed and then it'll block
		ws <- struct{}{}

		go func(i int, j job) {
			rs <- result{
				j.w(j.args...),
				i,
			}
			// Each time we've done some work we can signal that the next one can be processed
			<-ws
		}(i, j)
	}

	for i := 0; i < totalWork; i++ {
		r := <-rs

		results[r.position] = r.value
	}

	return results
}
