# Part 8 - don't panic

We haven't really done anything with error handling which perhaps is
ironic in Go, but this was to keep us focused. Now we'd like to make sure
we gracefully handle a function that panics.

You generally don't [panic](https://golang.org/doc/effective_go.html#panic)
yourself in Go and leave that to the runtime or an edge case that simply
should never happen but let's handle it anyway as a generic scheduler of
other people's work.

## Goal

* Using the same pattern from part 6 where you gracefully handle a timeout,
return a different error for work that panics without affecting the other
scheduled jobs.


## Acceptance criteria

* Schedule a non-panicking and panicking function which return with the
expected details

## Learning outcomes

* errors package and adding an external dependency
* recover
* Checking read channel values for zero value
* Wrapping errors
