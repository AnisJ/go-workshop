package part7

import (
	"errors"
	"runtime"
	"time"
)

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	maxgr   int
	timeout time.Duration
	jobs    []job
}

func NewScheduler(maxgr int, timeout time.Duration) *Scheduler {
	if maxgr == 0 {
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr:   maxgr,
		timeout: timeout,
	}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

type worker struct {
	job
	position int
}

type workStream chan worker

type workResult struct {
	value,
	position int
	err error
}

type resultStream chan workResult

func doWork(ws workStream, rs resultStream, cancel <-chan struct{}, timeout time.Duration) {
	for {
		select {
		case worker := <-ws:
			workStream := make(chan int, 1)

			go func() {
				workStream <- worker.w(worker.args...)
				close(workStream)
			}()

			select {
			case r := <-workStream:
				rs <- workResult{
					r,
					worker.position,
					nil,
				}
			case <-time.After(timeout):
				rs <- workResult{
					0,
					worker.position,
					Timeout,
				}
			}
		case <-cancel:
			return
		}
	}
}

type Result struct {
	Value int
	Err   error
}

var Timeout = errors.New("Timeout")

func (s *Scheduler) Run() []Result {
	var todo = make([]job, len(s.jobs))
	// We can make another slice based on the one we'd like to copy and then use copy to move the values into it
	copy(todo, s.jobs)
	// We then reset our scheduled jobs.
	s.jobs = make([]job, 0)

	totalWork := len(todo)
	results := make([]Result, totalWork)
	workStreams := make([]workStream, 0)
	currentWorker := 0
	rs := make(resultStream, totalWork)
	cancelWorkers := make(chan struct{})

	defer close(rs)
	defer close(cancelWorkers)

	for i, j := range todo {
		if len(workStreams) < s.maxgr {
			ws := make(workStream)
			workStreams = append(workStreams, ws)
			go doWork(ws, rs, cancelWorkers, s.timeout)
		}

		if currentWorker == len(workStreams) {
			currentWorker = 0
		}

		workStreams[currentWorker] <- worker{j, i}

		currentWorker++
	}

	for i := 0; i < totalWork; i++ {
		r := <-rs
		results[r.position] = Result{
			r.value,
			r.err,
		}
	}

	return results
}
