# Part 2 - do work

## Goal

* Add a method that allows us to run the scheduled functions with their
given arguments

## Acceptance criteria

* Proof that we get back the expected results from our given functions
in the order that the functions were scheduled and that they were called
with the correct arguments

## Bonus

* Write a benchmark test that shows how long your scheduler takes to
run pieces of work that take ~10ms to run

## Learning outcomes

* Private properties
* append
* make
* range
* Named/bare returns
* Blank values
* reflect package
* printf
* Benchmarking
* time package
