# Part 10 - Go wild

For simplicity we've been using the signature `func(...int) int`
which also allows us to get a `[]int` for our results.

This is overly restrictive though and we probably want to
schedule *any* work.

This is for educational purposes and `interface{}` should
be used sparingly due to the lack of type safety.

## Goal

* Change your implementation to allow us to schedule work
of `func() interface{}`

## Acceptance criteria

* Tests should pass after refactoring to use the new type
but without changing their assertions/intent

## Learning outcomes

* Functions as first class citizens
* The interface type
