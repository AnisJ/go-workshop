package part10

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"runtime"
	"time"
)

// You can group types in parentheses too
type (
	Result struct {
		Value interface{}
		Err   error
	}

	resultStream chan workResult

	Scheduler struct {
		maxgr   int
		timeout time.Duration
		jobs    []work
	}

	// We change our work to be a func that returns an unknown type (interface)
	// Now we no longer need args, we drop the concept of a `job`
	work func() interface{}

	worker struct {
		w        work
		position int
	}

	workResult struct {
		value    interface{}
		position int
		err      error
	}

	workStream chan worker
)

func NewScheduler(maxgr int, timeout time.Duration) *Scheduler {
	if maxgr == 0 {
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr:   maxgr,
		timeout: timeout,
	}
}

func (s *Scheduler) Add(w work) {
	s.jobs = append(s.jobs, w)
}

func doWork(worker worker, timeout time.Duration) workResult {
	workStream := make(chan interface{}, 1)
	rs := make(resultStream, 1)
	defer close(rs)

	go func() {
		defer func() {
			if r := recover(); r != nil {
				rs <- workResult{
					0,
					worker.position,
					wrapPanic(fmt.Errorf(r.(string))),
				}
			}
		}()
		defer close(workStream)

		workStream <- worker.w()
	}()

	ctx, cancel := context.WithTimeout(context.Background(), timeout)

	select {
	case r, ok := <-workStream:
		cancel()
		if ok {
			rs <- workResult{
				r,
				worker.position,
				nil,
			}
		}
	case <-ctx.Done():
		rs <- workResult{
			0,
			worker.position,
			Timeout,
		}
	}

	return <-rs
}

var Timeout = errors.New("Timeout")

func wrapPanic(err error) error {
	return errors.Wrap(err, "Panicked")
}

func (s *Scheduler) Run() []Result {
	var todo = make([]work, len(s.jobs))
	copy(todo, s.jobs)
	s.jobs = make([]work, 0)

	totalWork := len(todo)
	results := make([]Result, totalWork)
	workStreams := make([]workStream, 0)
	currentWorker := 0
	rs := make(resultStream, totalWork)
	cancelWorkers := make(chan struct{})

	defer close(rs)
	defer close(cancelWorkers)

	for i, j := range todo {
		if len(workStreams) < s.maxgr {
			ws := make(workStream)
			workStreams = append(workStreams, ws)

			go func() {
				for {
					select {
					case worker := <-ws:
						rs <- doWork(worker, s.timeout)
					case <-cancelWorkers:
						return
					}
				}
			}()
		}

		if currentWorker == len(workStreams) {
			currentWorker = 0
		}

		workStreams[currentWorker] <- worker{j, i}

		currentWorker++
	}

	for i := 0; i < totalWork; i++ {
		r := <-rs
		results[r.position] = Result{
			r.value,
			r.err,
		}
	}

	return results
}
